export const prettyDate = (uglyDate: string): string => {
  const date = new Date(uglyDate);
  const day = String(date.getDate());
  const month = String(date.getMonth() + 1);
  const year = date.getFullYear();

  const prettyDay = day.length > 1 ? day : '0' + day;
  const prettyMonth = month.length > 1 ? month : '0' + month;

  return `${prettyDay}.${prettyMonth}.${year}`;
};
