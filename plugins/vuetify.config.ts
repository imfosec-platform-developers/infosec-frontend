export default {
  theme: {
    dark: true,
    themes: {
      dark: {
        primary: '#1a5cff',
        success: '#46c93a',
        error: '#ff4757',
        warning: '#ff8c00',
      },
    },
  },
};
