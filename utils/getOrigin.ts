import env from '@/env';

const { isDev, serverURL } = env;

export const getOrigin = () => (isDev && serverURL ? serverURL : '');
