export interface IToastConfig {
  text: string;
  timeout?: number;
  left?: boolean;
  right?: boolean;
  top?: boolean;
  bottom?: boolean;
}

export const SHOW_TOAST_EVENT_NAME = 'hitShowToast';
export const HIDE_TOAST_EVENT_NAME = 'hitHideToast';

export const showToast = (config: IToastConfig) => {
  window.$nuxt.$emit(SHOW_TOAST_EVENT_NAME, config);
};

export const hideToast = () => {
  window.$nuxt.$emit(HIDE_TOAST_EVENT_NAME);
};
