import marked from 'marked';
import { sanitize } from 'dompurify';

export const mdToHTML = (md: string) => {
  const dirtyHTML = marked(md);
  const cleanHTML = sanitize(dirtyHTML);

  return cleanHTML;
};
