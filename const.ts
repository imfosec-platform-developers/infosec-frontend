import { ChallengeCategory, ChallengeDifficulty } from '@/dataProvider';

export const categories: ChallengeCategory[] = [
  'crypto',
  'forensic',
  'linux',
  'network',
  'reverse',
  'web',
];

export const difficultyLevels: ChallengeDifficulty[] = [
  'easy',
  'hard',
  'impossible',
  'medium',
];
