// @ts-ignore
export default function ({ route, store, redirect }) {
  if (
    route.name === 'join' ||
    route.name === 'login' ||
    store.state.auth.loggedIn
  ) {
    return;
  }
  redirect('/login');
}
