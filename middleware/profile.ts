import { FETCH_PROFILE } from '@/store/auth';

// @ts-ignore
export default function ({ store }) {
  if (!store.state.auth.loggedIn) {
    return;
  }
  store.dispatch(FETCH_PROFILE);
}
