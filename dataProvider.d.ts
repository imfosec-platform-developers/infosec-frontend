/* eslint-disable camelcase */

/**
 * Типы ролей
 */
export type UserRole = 'user' | 'admin' | 'editor';
/**
 * Данные необхадимые для входа
 */
export interface ILoginCreditinals {
  username: string;
  password: string;
}

/**
 * Данные которые возвращает /users/token
 */
export interface ITokenReturns {
  access_token: string;
  token_type: string;
}

/**
 * Данные которые принимает /users/register
 */
export interface IRegistrationBody {
  username: string;
  password: string;
  email?: string;
  full_name?: string;
  // Может расширяться так как сервер принимает много опций
}

/**
 * Объект пользователя приходящий с сервера
 */
export interface IUser {
  username: string;
  email: string | null;
  full_name: string | null;
  sex: number | null;
  disabled: boolean | null;
  user_role: UserRole;
  avatar_path: string;
  created_at: string;
  modified_at: string | null;
  solved_challenges_id: {};
  num_of_solved_challenges: number;
  place_in_scoreboard: number;
  users_score: number;
  users_group: null | string; // Уточнить что за поле
}

/**
 * Варианты категорий для заданий
 */
export type ChallengeCategory =
  | 'web'
  | 'crypto'
  | 'forensic'
  | 'network'
  | 'linux'
  | 'reverse'
  | '';

/**
 * Варианты сложности для заданий
 */
export type ChallengeDifficulty =
  | 'easy'
  | 'medium'
  | 'hard'
  | 'impossible'
  | '';

/**
 * Объект приходящий при запросе по шортнейму
 */
export interface ISingleChallenge {
  author: string;
  category: ChallengeCategory;
  challenge_created: string;
  challenge_modified?: string;
  difficulty_rating?: string;
  difficulty_tag: ChallengeDifficulty;
  first_blood?: string;
  flag: string;
  score: number;
  shortname: string;
  solutions_num: number;
  tags: string[];
  text: string;
  title: string;
  useful_resources: string[];
  wrong_solutions_num: number;
  solved: boolean;
}

/**
 * Объект задания приходящий с сервера
 */
export interface IChallenge {
  title: string;
  shortname: string;
  score: number;
  tags: string[];
  category: ChallengeCategory;
  author?: string;
  difficulty_tag: ChallengeDifficulty;
  challenge_created: string;
  solved: boolean;
}

/**
 * Объект пользователя на таблице лидеров
 */
export interface IScoreboardUser {
  username: string;
  users_score: number;
  users_group: string;
  num_of_solved_challenges: number;
  place_in_scoreboard: number;
}

/**
 * Объект часто задаваемого вопроса
 */
export interface IFAQ {
  _id: string;
  question: string;
  answer: string;
}

/**
 * Объект спонсора
 */
export interface ISponsor {
  _id: string;
  title: string;
  description: string;
  img: string;
}

export interface IWriteup {
  /** При запросе множества врайтапов */
  _id: string;
  /** При ответе на обновление score */
  id: string;
  author: string;
  challenge_shortname: string;
  score: number;
  text: string;
  writeup_created: string;
  is_owner: boolean;
}
