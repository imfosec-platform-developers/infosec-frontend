/**
 * Тип кастомных иконок
 */
export type IconType =
  | 'plus'
  | 'mail'
  | 'file'
  | 'document'
  | 'cross'
  | 'menu-burger'
  | 'check-mark';

export type BootsrapColumnNumber =
  | 1
  | 2
  | 3
  | 4
  | 5
  | 6
  | 7
  | 8
  | 9
  | 10
  | 11
  | 12;

export interface ISizes {
  width: string;
  height: string;
}

/**
 * Интерфейс для ссылок на страницы
 */
export interface IPageLink {
  title: string;
  link: string;
}

/**
 * Интерфейс для загаловка таблицы Vuetify
 */
export interface IVuetifyTableHeader {
  text: string;
  value: string;
  align?: 'start' | 'center' | 'end';
  sortable?: boolean;
  filterable?: boolean;
  groupable?: boolean;
  divider?: boolean;
  class?: string | string[];
  cellClass?: string | string[];
  width?: string | number;
  filter?: (value: any, search: string, item: any) => boolean;
  sort?: (a: any, b: any) => number;
}

/**
 * Интерфейс для приходящих с сервера заданий
 */
export interface ITask {
  id: number;
  title: string;
  points: number;
  date: string;
  author: string;
  tags: string[];
  category: string;
}
