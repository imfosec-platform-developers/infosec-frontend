export default {
  serverURL: process.env.SERVER_URL,
  isDev: process.env.NODE_ENV === 'development',
};
