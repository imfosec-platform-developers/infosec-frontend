import { GetterTree, ActionTree, MutationTree } from 'vuex';

import { RootState } from '@/store/index';
import { fetchContacts } from '@/api/contacts';

/** Убираем префикс перед обращением */
export const namespaced = false;

export const GET_TELEGRAM = 'GET_TELEGRAM';
export const SET_TELEGRAM = 'SET_TELEGRAM';
export const FETCH_CONTACTS = 'FETCH_CONTACTS';

export const state = () => ({
  telegram: '',
});

export type ContactState = ReturnType<typeof state>;

export const getters: GetterTree<ContactState, RootState> = {
  [GET_TELEGRAM]: (state) => {
    return state.telegram;
  },
};

export const mutations: MutationTree<ContactState> = {
  [SET_TELEGRAM]: (state, link: string) => {
    state.telegram = link;
  },
};

export const actions: ActionTree<ContactState, RootState> = {
  [FETCH_CONTACTS]: async ({ commit }) => {
    try {
      const { data } = await fetchContacts();

      commit(SET_TELEGRAM, data['telegram:']);
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
    }
  },
};
