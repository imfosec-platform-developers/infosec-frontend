import { GetterTree, ActionTree, MutationTree } from 'vuex';
import { IUser, ILoginCreditinals, ITokenReturns } from '../dataProvider';
import { RootState } from './index';
import { getToken, setToken, clearToken } from '~/api/token';
import { fetchToken, fetchProfile } from '~/api/auth';

/** Убираем префикс перед обращением */
export const namespaced = false;

/** Устанавливает что пользователя залогинен */
export const SET_LOGGED_IN = 'SET_LOGGED_IN';
/** Устанавливает что пользователя не залогинен */
export const SET_LOGGED_OUT = 'SET_LOGGED_OUT';
/** Устанавливает профиль пользователя */
export const SET_USER = 'SET_USER';
/** Очищает профиль пользователя */
export const CLEAR_USER = 'CLEAR_USER';
/** Получает состояние залогин/незалогин */
export const GET_LOGGED_IN = 'GET_LOGGED_IN';
/** Получает состояние пользователя */
export const GET_USER = 'GET_USER';
/** Функция залогина */
export const LOGIN = 'LOGIN';
/** Функция получения профиля */
export const FETCH_PROFILE = 'FETCH_PROFILE';
/** Функция выхода */
export const LOGOUT = 'LOGOUT';

export const state = () => ({
  loggedIn: Boolean(getToken()),
  user: {} as IUser | {},
});

export type AuthState = ReturnType<typeof state>;

export const getters: GetterTree<AuthState, RootState> = {
  [GET_LOGGED_IN]: (state) => state.loggedIn,
  [GET_USER]: (state) => state.user,
};

export const mutations: MutationTree<AuthState> = {
  [SET_USER]: (state, newUser: IUser) => {
    state.user = newUser;
  },
  [CLEAR_USER]: (state) => {
    state.user = {};
  },
  [SET_LOGGED_IN]: (state, token: string) => {
    setToken(token);
    state.loggedIn = true;
  },
  [SET_LOGGED_OUT]: (state) => {
    clearToken();
    state.loggedIn = false;
  },
};

export const actions: ActionTree<AuthState, RootState> = {
  async [LOGIN](
    { commit },
    payload: ILoginCreditinals
  ): Promise<ITokenReturns> {
    const { data } = await fetchToken(payload);

    commit(SET_LOGGED_IN, data.access_token);

    const { data: user } = await fetchProfile();

    commit(SET_USER, user);

    return data;
  },
  async [FETCH_PROFILE]({ commit }): Promise<IUser> {
    const { data } = await fetchProfile();

    commit(SET_USER, data);

    return data;
  },
  [LOGOUT]({ commit }): void {
    commit(SET_LOGGED_OUT);
    commit(CLEAR_USER);
  },
};
