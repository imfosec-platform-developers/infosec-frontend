/* eslint-disable camelcase */
import { AxiosPromise } from 'axios';
import { $http } from './http';
import {
  IChallenge,
  ChallengeCategory,
  ChallengeDifficulty,
  ISingleChallenge,
} from '~/dataProvider';

export interface IFetchChallengesOptions {
  category?: ChallengeCategory[];
  pageCount?: number | string;
  rowCount?: number;
  difficult: ChallengeDifficulty[];
  tags: string[];
  title?: string;
  dateSort?: 1 | -1;
}

export interface IFetchChallengesReturns {
  challenges: IChallenge[];
  num_of_challenges: number;
}

interface ISubmitFlagOptions {
  shortname: string;
  flag: string;
}

interface ISubmitFlagReturns {
  flag: boolean;
  challenge: ISingleChallenge;
}

export const fetchChallenges = ({
  category,
  pageCount = 1,
  rowCount = 10,
  difficult,
  tags,
  dateSort = 1,
  title,
}: IFetchChallengesOptions): AxiosPromise<IFetchChallengesReturns> => {
  const searchQuery = title ? `&title=${title}` : '';
  const dateSortQuery = dateSort === -1 ? '&date=-1' : '';
  const url = `/challenges/list?page_count=${pageCount}&row_count=${rowCount}${dateSortQuery}${searchQuery}`;

  return $http.post(url, { difficult, tags, category });
};

export const fetchMyChallenges = (
  pageCount = 1,
  rowCount = 10
): AxiosPromise<IFetchChallengesReturns> => {
  return $http.get(
    `/challenges/my_solved_challenges?page_number=${pageCount}&row_count=${rowCount}`
  );
};

export const fetchChallengeMarkup = (
  shortname: string
): AxiosPromise<ISingleChallenge> => {
  return $http.get(`/challenges/get_challenge?shortname=${shortname}`);
};

export const submitFlag = (
  payload: ISubmitFlagOptions
): AxiosPromise<ISubmitFlagReturns> => {
  return $http.post('/flag/submit_flag', payload);
};
