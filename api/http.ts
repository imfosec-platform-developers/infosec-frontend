import axios from 'axios';

import env from '@/env.js';
import { CLEAR_USER, SET_LOGGED_OUT } from '@/store/auth';

import { getToken } from './token';

export const $http = axios.create({
  baseURL: env.serverURL,
});

$http.interceptors.request.use((request) => {
  request.headers.Authorization = getToken();

  return request;
});

$http.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error.response.status === 401) {
      window.$nuxt.$store.commit(SET_LOGGED_OUT);
      window.$nuxt.$store.commit(CLEAR_USER);

      window.$nuxt.$router.push({ path: '/login' });
    }

    throw error;
  }
);
