/* eslint-disable camelcase */
import { AxiosPromise } from 'axios';
import { $http } from './http';
import { IScoreboardUser } from '~/dataProvider';

export interface IFetchScoreBoardReturns {
  num_of_users: number;
  num_of_challenges: number;
  scoreboard: IScoreboardUser[];
}

export const fetchScoreboard = (
  pageCount = 1,
  rowCount = 10
): AxiosPromise<IFetchScoreBoardReturns> => {
  return $http.post(
    `/scoreboard?page_number=${pageCount}&row_count=${rowCount}`
  );
};
