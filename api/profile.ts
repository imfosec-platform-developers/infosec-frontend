/* eslint-disable camelcase */
import { AxiosPromise } from 'axios';

import { $http } from './http';

import { IUser } from '~/dataProvider';

// Интерфейс для формирования запроса на измениие собственных данных пользователя
export interface IProfileChangeOptions {
  username?: string;
  email?: string;
  full_name?: string;
}

// Интерфейс для получения ответа об измении собственных данных пользователя
export interface IProfileChangeReturns {
  status: string;
}

export const changeProfileData = (
  data: IProfileChangeOptions
): AxiosPromise<IProfileChangeReturns> => {
  return $http.post('/users/change', data);
};

export const fetchCurrentUser = (username: string): AxiosPromise<IUser> => {
  return $http.get(`/users/info?username=${username}`);
};
