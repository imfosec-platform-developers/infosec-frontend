/* eslint-disable camelcase */
import { AxiosPromise } from 'axios';

import { IWriteup } from '@/dataProvider';

import { $http } from './http';

export interface IFetchWriteupsReturns {
  writeups: IWriteup[];
}

export interface ICreateWriteupOptions {
  challenge_shortname: string;
  text: string;
}

export interface IDeleteWriteupReturns {
  status: string;
}

export interface IUpdateWriteupOptions {
  challenge_shortname: string;
  text: string;
}

export interface IUpdateScoreOptions {
  challenge_shortname: string;
  author: string;
  value: 1 | -1;
}

export interface IUpdateScoreReturns {
  score: number;
  status: boolean;
  writeup: IWriteup;
}

export const fetchWriteups = (
  shortname: string
): AxiosPromise<IFetchWriteupsReturns> => {
  return $http.get(`writeup/?shortname=${shortname}`);
};

export const createWriteup = (
  options: ICreateWriteupOptions
): AxiosPromise<IWriteup> => {
  return $http.put(`writeup/new`, options);
};

export const deleteWriteup = (
  shortname: string
): AxiosPromise<IDeleteWriteupReturns> => {
  return $http.delete(`writeup/delete?challenge_shortname=${shortname}`);
};

export const updateWiteup = (
  options: IUpdateWriteupOptions
): AxiosPromise<IUpdateScoreReturns> => {
  return $http.post('writeup/update', options);
};

export const updateScore = (
  options: IUpdateScoreOptions
): AxiosPromise<IUpdateScoreReturns> => {
  return $http.post('writeup/score', options);
};
