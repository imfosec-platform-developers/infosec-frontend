/* eslint-disable camelcase */
import { AxiosPromise } from 'axios';

import { $http } from '@/api/http';

export interface IChangePasswordReturns {
  username: string;
  email: string;
  full_name: string;
  sex: null;
  disabled: boolean;
  user_role: string;
  avatar_path: string;
  created_at: string;
  modified_at: string;
}

export const changePassword = (
  oldPassword: string,
  newPassword: string
): AxiosPromise<IChangePasswordReturns> => {
  return $http.post('/users/change_password', {
    old_password: oldPassword,
    new_password: newPassword,
  });
};
