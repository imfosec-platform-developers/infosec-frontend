/* eslint-disable camelcase */
import { AxiosPromise } from 'axios';

import { $http } from '@/api/http';

export interface IChangeAvatarReturns {
  status: string;
}

export const changeAvatar = (
  avatarFile: File
): AxiosPromise<IChangeAvatarReturns> => {
  const formData = new FormData();
  formData.append('avatar_img', avatarFile);

  return $http.put('/users/upload_avatar', formData, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
};
