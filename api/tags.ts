import { AxiosPromise } from 'axios';

import { $http } from './http';

export interface IFetchTagsReturns {
  // eslint-disable-next-line camelcase
  tags_list: string[];
}

export const fetchTags = (): AxiosPromise<IFetchTagsReturns> => {
  return $http.get('/challenges/tags_list');
};
