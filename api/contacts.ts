import { AxiosPromise } from 'axios';
import { $http } from './http';

export interface IContacts {
  'telegram:': string;
}

export const fetchContacts = (): AxiosPromise<IContacts> => {
  return $http.get('/info/contacts');
};
