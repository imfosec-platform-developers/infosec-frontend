import { AxiosPromise } from 'axios';
import { $http } from './http';
import { IFAQ } from '~/dataProvider';

export const fetchFAQs = (): AxiosPromise<IFAQ[]> => {
  return $http.get('/faq/list');
};
