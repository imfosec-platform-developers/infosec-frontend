import { AxiosPromise } from 'axios';
import { $http } from './http';
import {
  ILoginCreditinals,
  IRegistrationBody,
  ITokenReturns,
  IUser,
} from '~/dataProvider';

/**
 * Функция для входа
 * @param creditinals {ILoginCreditinals} Данные для входа
 * @returns {Promise} Промис запроса
 */
export const fetchToken = (
  creditinals: ILoginCreditinals
): AxiosPromise<ITokenReturns> => {
  const formData = new FormData();
  formData.append('username', creditinals.username);
  formData.append('password', creditinals.password);

  return $http.post('/users/token', formData);
};

/**
 * Функция для регистрации нового пользователя
 * @param newUser {IRegistrationBody} Данные для регистрации
 * @returns {Promise} Промис запроса
 */
export const createUser = (newUser: IRegistrationBody): AxiosPromise => {
  return $http.put('/users/register', newUser);
};

/**
 * Функция получения профиля пользователя
 * @returns {Promis} Промис запроса
 */
export const fetchProfile = (): AxiosPromise<IUser> => {
  return $http.get('/users/me');
};
