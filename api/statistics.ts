import { AxiosPromise } from 'axios';

import { $http } from './http';

export interface IStatistics {
  // eslint-disable-next-line camelcase
  num_of_users: number;
  // eslint-disable-next-line camelcase
  num_of_challenges: number;
}

export interface IFetchStatisticsReturns extends IStatistics {}

export const fetchStatistics = (): AxiosPromise<IFetchStatisticsReturns> => {
  return $http.get('/stats/');
};
