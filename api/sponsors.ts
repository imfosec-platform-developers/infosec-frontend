import { AxiosPromise } from 'axios';
import { $http } from './http';
import { ISponsor } from '~/dataProvider';

export const fetchSponsors = (): AxiosPromise<ISponsor[]> => {
  return $http.get('/sponsors/list');
};
