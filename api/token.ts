export const TOKEN_KEY = 'token';
/**
 * Функция получения токена из LocalStorage
 * @returns {string} Токен
 */
export const getToken = (): string => localStorage.getItem(TOKEN_KEY) || '';

/**
 * Функция установки токена в LocalStorage
 * @param newToken {string} Новый токен
 */
export const setToken = (newToken: string): void => {
  const tokenValue = `Bearer ${newToken}`;
  localStorage.setItem(TOKEN_KEY, tokenValue);
};

/**
 * Функция очистки токена
 */
export const clearToken = (): void => {
  localStorage.removeItem(TOKEN_KEY);
};
